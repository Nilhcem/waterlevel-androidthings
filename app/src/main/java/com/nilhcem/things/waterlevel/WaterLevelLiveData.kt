package com.nilhcem.things.waterlevel

import android.arch.lifecycle.LiveData
import android.os.Handler
import android.os.HandlerThread
import com.google.android.things.pio.PeripheralManagerService
import com.google.android.things.pio.UartDevice
import com.google.android.things.pio.UartDeviceCallback

class WaterLevelLiveData : LiveData<Int>() {

    companion object {
        private val TAG = WaterLevelLiveData::class.java.simpleName!!
        private const val UART_NAME = "UART3"
    }

    private var handler: Handler? = null
    private var handlerThread: HandlerThread? = null

    private var uartDevice: UartDevice? = null
    private val uartBuffer = ByteArray(32)

    private val callback = object : UartDeviceCallback() {
        override fun onUartDeviceDataAvailable(uart: UartDevice): Boolean {
            val read = uart.read(uartBuffer, uartBuffer.size)
            postValue(String(uartBuffer, 0, read, Charsets.UTF_8).trim().toInt())
            return true
        }

        override fun onUartDeviceError(uart: UartDevice, error: Int) {}
    }

    override fun onActive() {
        openUart()
    }

    override fun onInactive() {
        closeUart()
    }

    private fun openUart() {
        handlerThread = HandlerThread(TAG).also { handlerThread ->
            handlerThread.start()
            handler = Handler(handlerThread.looper)
        }

        uartDevice = PeripheralManagerService().openUartDevice(UART_NAME).apply {
            setBaudrate(9600)
            setDataSize(8)
            setParity(UartDevice.PARITY_NONE)
            setStopBits(1)

            registerUartDeviceCallback(callback, handler)
        }
    }

    private fun closeUart() {
        handler = null
        handlerThread?.quitSafely().also { handlerThread = null }

        uartDevice?.apply {
            unregisterUartDeviceCallback(callback)
            close()
        }.also { uartDevice = null }
    }
}
