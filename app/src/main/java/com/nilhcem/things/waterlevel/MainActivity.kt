package com.nilhcem.things.waterlevel

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        WaterLevelLiveData().observe({ lifecycle }) { waterLevel ->
            Log.i(MainActivity::class.java.simpleName!!, "Water level: $waterLevel")
        }
    }
}
